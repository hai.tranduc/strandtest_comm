/* Define function to command with PC.
*/

#ifndef ARDUINO_BRIDGE_H
#define ARDUINO_BRIDGE_H

#include "commands.h"

// A pair of varibles to help parse serial commands (thanks Fergs)
int arg = 0;
int _index = 0;

// Variable to hold an input character
char chr;

// Variable to hold the current single-character command
char cmd;

// Character arrays to hold the first and second arguments
char sum_arr[5];
char argv1[16];
char argv2[16];

// The arguments converted to integers
long arg0;
long arg1;
long arg2;

// Sum check
int sum_calr = 0;
int sum_recv = 0;

// For logger
bool logger_req = false;
bool logger_busy = false;
uint32_t log_key = 0;
String log_msg = "";
String log_confirm = "";
int log_step = 99;

/* Clear the current command parameters */
void resetCommand()
{
   cmd = NULL;
   memset(sum_arr, 0, sizeof(sum_arr));
   memset(argv1, 0, sizeof(argv1));
   memset(argv2, 0, sizeof(argv2));
   arg1 = 0;
   arg2 = 0;
   arg = 0;
   _index = 0;
   sum_calr = 0;
   sum_recv = 0;
}

/* Run a command.  Commands are defined in commands.h */
int runCommand()
{
   static String toprint = "";
   arg1 = atoi(argv1);
   arg2 = atoi(argv2);
   String str;

   switch(cmd) {
   case GET_BAUDRATE:
      Serial.println(BAUDRATE);
      break;
   case ANALOG_READ:
      Serial.println(analogRead(arg1));
      break;
   case DIGITAL_READ:
      Serial.println(digitalRead(arg1));
      break;
   case ANALOG_WRITE:
      analogWrite(arg1, arg2);
      Serial.println("OK"); 
      break;
   case DIGITAL_WRITE:
      if (arg2 == 0) digitalWrite(arg1, LOW);
      else if (arg2 == 1) digitalWrite(arg1, HIGH);
      Serial.println("OK"); 
      break;
   case PIN_MODE:
      if (arg2 == 0) pinMode(arg1, INPUT);
      else if (arg2 == 1) pinMode(arg1, OUTPUT);
      Serial.println("OK");
      break;
   case LED_WRITE:
      ledWrite(arg1); 
      Serial.println("OK"); 
      break; 
   case LOG_CONFIRM:
      log_confirm = "";
      log_confirm = argv1;
      Serial.println(log_confirm);
      break;
   case CARD_ID:
      Serial.println("0123456789");
      break;
   case MOTOR_SPEEDS: // Also use to keep alive and send log from Arduino
      if (logger_req) // Arduino not yet sure host PC received log
      {
         toprint = "";
         toprint += log_key;  
         toprint += '$';
         toprint += log_msg;
         Serial.println(toprint);
         logger_req = false;
         log_key = 0;
      }
      else
      {
         str = String("Motor: ");
         str += atof(argv1);
         str += " ";
         str += atof(argv2);
         Serial.println(str);
      }
      
      break;
   default:
      Serial.println("Invalid Command");
      break;
   }
}

// Arduino sure that host PC received log successfully
void sendLogPool()
{
   static uint32_t t = millis();
   static int attempts = 0;
   static String toprint = "";
   static uint32_t key = millis();
   
   switch(log_step) {
   case 0:
      attempts = 0;
      log_step = 1;
      key = millis();
      // Do not break
   case 1:
      toprint = "";
      toprint += attempts;
      toprint += '_';
      toprint += key;  
      toprint += '$';
      toprint += log_msg;
      Serial.println(toprint);
      log_confirm = "";
      t = millis();
      log_step = 2;
      break;
   case 2:
      if (log_confirm == log_msg)
      {
         log_step = 99;
      }
      else if (millis() - t > 200)
      {
         log_step = 1;
         attempts ++;
      }
   }
}

// void sendLog(String msg)
// {
//    log_msg = msg;
//    log_step = 0;
// }

void sendLog(String msg)
{
   log_msg = msg;
   logger_req = true;
   log_key = millis();
}

void rosLoop()
{
   static String toprint = "";

   while (Serial.available() > 0)
   {
      // Read the next character
      chr = Serial.read();

      // Terminate a command with a CR
      if (chr == 13)
      {
         sum_recv = atoi(sum_arr);
         sum_calr &= 0x00FF;
         // Serial.print(sum_recv);
         // Serial.print(" ");
         // Serial.println(sum_calr);
         
         if (arg == 1)
         {
            sum_arr[_index] = NULL;
         } 
         else if (arg == 2)
         {
            argv1[_index] = NULL;
         }
         else if (arg == 3)
         {
            argv2[_index] = NULL;
         }
         if (sum_recv == sum_calr)
         {
            runCommand();
         }
         else
         {
            Serial.println("SUM_ERR");
         }
         resetCommand();
      }
      // Use # to delimit parts of the command
      else if (chr == '#')
      {
         // Step through the arguments
         if (arg == 0)
         {
            arg = 1;
         }
         else if (arg == 1)  
         {
            sum_arr[_index] = NULL;
            arg = 2;
            _index = 0;
         }
         else if (arg == 2)  
         {
            argv1[_index] = NULL;
            arg = 3;
            _index = 0;
         }
         // return;     // use if
         continue;   // use while
      }
      else
      {
         if (arg == 0)
         {
            // The first arg is the single-letter command
            cmd = chr;
         }
         else if (arg == 1)
         {
            // Subsequent arguments can be more than one character
            sum_arr[_index] = chr;
            _index+=1;
         }
         else if (arg == 2)
         {
            // Subsequent arguments can be more than one character
            argv1[_index] = chr;
            _index+=1;
         }
         else if (arg == 3)
         {
            argv2[_index] = chr;
            _index+=1;
         }
         // Calr check sum: cmd + arg1 + arg2 + ...
         if (arg ==0 or arg > 1)
         {
            sum_calr += chr;
         }
      }
   }
   // sendLogPool();
}

#endif


