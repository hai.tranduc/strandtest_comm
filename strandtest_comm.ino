// A basic everyday NeoPixel strip test program.

// NEOPIXEL BEST PRACTICES for most reliable operation:
// - Add 1000 uF CAPACITOR between NeoPixel strip's + and - connections.
// - MINIMIZE WIRING LENGTH between microcontroller board and first pixel.
// - NeoPixel strip's DATA-IN should pass through a 300-500 OHM RESISTOR.
// - AVOID connecting NeoPixels on a LIVE CIRCUIT. If you must, ALWAYS
//   connect GROUND (-) first, then +, then data.
// - When using a 3.3V microcontroller with a 5V-powered NeoPixel strip,
//   a LOGIC-LEVEL CONVERTER on the data line is STRONGLY RECOMMENDED.
// (Skipping these may work OK on your workbench but can fail in the field)

#include <Adafruit_NeoPixel.h>
#include "define.h"

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN    6

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 60

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRBW + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)


// setup() function -- runs once at startup --------------------------------

bool NewState = false;

// ROS
#ifdef USE_ROSSERIAL
#include <ros.h>
#include <std_msgs/Int16.h>
ros::NodeHandle nh;
void led_control_cb(const std_msgs::Int16 &led_control_msg);
ros::Subscriber<std_msgs::Int16> led_control_sub("/led_control", &led_control_cb);
int16_t LedState = 0;
#endif

#ifdef USE_ARDUINO_BRIDGE
#include "arduino_bridge.h"
int LedState = 0;
#endif

#ifdef USE_MODBUS
#include <ModbusRtu.h>

uint16_t au16data[16] = {  1, 1415, 9265, 4, 2, 7182, 28182, 8, 0, 0, 0, 0, 0, 0, 1, -1 }; // data array for modbus network sharing
Modbus slave(1,0,0); // this is slave @1 and RS-232 or USB-FTDI
int LedState = 0;
#endif

#ifdef USE_RTOS
#include <Arduino_FreeRTOS.h>

void TaskLoop( void *pvParameters );
void TaskModbus( void *pvParameters );
#endif

// Use if a led effect need more step
int LedStep = 0;

void setup() {
  // These lines are specifically to support the Adafruit Trinket 5V 16 MHz.
  // Any other board, you can remove this part (but no harm leaving it):
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)
  pinMode(LED_BUILTIN, OUTPUT);

#ifdef USE_ROSSERIAL
  nh.getHardware()->setBaud(BAUDRATE);
  nh.initNode();
  nh.subscribe(led_control_sub);
#endif

#ifdef USE_ARDUINO_BRIDGE
  Serial.begin(BAUDRATE);
#endif

#ifdef USE_MODBUS
  // 01 06 00 00 00 00 89 CA
  // 01 06 00 00 00 01 48 0A
  // 01 06 00 00 00 02 08 0B
  // slave.begin(19200);
  slave.begin(19200, SERIAL_8E1); // 19200 baud, 8-bits, even, 1-bit stop
#endif

#ifdef USE_RTOS
  xTaskCreate(
    TaskLoop
    ,  "Loop"   // A name just for humans
    ,  128  // This stack size can be checked & adjusted by reading the Stack Highwater
    ,  NULL
    ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    ,  NULL );

  xTaskCreate(
    TaskModbus
    ,  "Modbus"
    ,  128  // Stack size
    ,  NULL
    ,  1  // Priority
    ,  NULL );
#endif
}

#ifdef USE_RTOS
void TaskLoop(void *pvParameters)  // This is a task.
{
  (void) pvParameters;
  uint32_t t = millis();

  for (;;) // A Task shall never return or exit.
  {
    if (millis() - t >= 500)
    {
      digitalWrite(13, !digitalRead(13));
      t = millis();
    }
    ledSwitch(LedState);
    vTaskDelay(20 / portTICK_PERIOD_MS);
  }
}

void TaskModbus(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  for (;;)
  {
    slave.poll(au16data, 16);
    LedState = au16data[0];
  }
}
#endif

// loop() function -- runs repeatedly as long as board is on ---------------

void loop() 
{
  static uint32_t t = millis();
#ifndef USE_RTOS

#ifdef USE_ROSSERIAL
  nh.spinOnce();
#endif

#ifdef USE_ARDUINO_BRIDGE
  rosLoop();
#endif

#ifdef USE_MODBUS
  slave.poll(au16data, 16);
  LedState = au16data[0];
#endif

  ledSwitch(LedState);
  if (millis() - t >= 200)
  {
    t = millis();
    // Serial.println(t);
  }
#endif

  // Fill along the length of the strip in various colors...
  // colorWipe(strip.Color(255,   0,   0), 50); // Red
  // colorWipe(strip.Color(  0, 255,   0), 50); // Green
  // colorWipe(strip.Color(  0,   0, 255), 50); // Blue
  // colorWipe(strip.Color(  0,   0,   0, 255), 50); // True white (not RGB white)

  // // Do a theater marquee effect in various colors...
  // theaterChase(strip.Color(127, 127, 127), 50); // White, half brightness
  // theaterChase(strip.Color(127,   0,   0), 50); // Red, half brightness
  // theaterChase(strip.Color(  0,   0, 127), 50); // Blue, half brightness

  // rainbow(10);             // Flowing rainbow cycle along the whole strip
  // theaterChaseRainbow(50); // Rainbow-enhanced theaterChase variant

  // use NEO_GRBW instead NEO_GRB
  // rainbowFade2White(3, 3, 1);
  // whiteOverRainbow(75, 5);
  // pulseWhite(5);
}

#ifdef USE_ROSSERIAL
void led_control_cb(const std_msgs::Int16 &led_control_msg)
{
  LedState = led_control_msg.data;
  NewState = true;
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
}
#endif

#ifdef USE_ARDUINO_BRIDGE
void ledWrite(int state)
{
  if (LedState != state)
  {
    NewState = true;
  }
  LedState = state;
  LedStep = 0;
}
#endif

void ledSwitch(int state)
{
  switch (state)
  {
  case 0:
    if (myColorWipe(strip.Color(255,   0,   0), 10))
    {
      LedState = 99;
      // log_msg = "Led red done";
      // log_step = 0;
      sendLog("Led red done");
    }
    break;
  case 1:
    if (myColorWipe(strip.Color(0,   255,   0), 5))
    {
      LedState = 99;
      // log_msg = "Led green done";
      // log_step = 0;
      sendLog("Led green done");
    }
    break;
  case 2:
    if (myRainbow(5))
    {
      // log_msg = "Led rainbow done";
      // log_step = 0;
      sendLog("Led rainbow done");
    }
    break;
  default:
    break;
  }
}

// Some functions of our own for creating animated effects -----------------

// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
bool colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
  return true;
}

bool myColorWipe(uint32_t color, int wait) {
  static uint32_t t = millis();
  static int i = 0;
  
  if (NewState)
  {
    NewState = false;
    i = 0;
    // return true;
  }
  
  if (i <= strip.numPixels())
  {
    if (millis() - t >= wait)
    {
      strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
      strip.show();                          //  Update strip to match
      if (i++ == strip.numPixels()) 
      {
        i = 0;
        return true;
      }     
      // digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      t = millis();
    }
  }
  return false;
}

// Theater-marquee-style chasing lights. Pass in a color (32-bit value,
// a la strip.Color(r,g,b) as mentioned above), and a delay time (in ms)
// between frames.
void theaterChase(uint32_t color, int wait) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show(); // Update strip with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.
void rainbow(int wait) {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
    delay(wait);  // Pause for a moment
  }
}

bool myRainbow(int wait) {
  static uint32_t t = millis();
  static int i = 0;
  static long firstPixelHue = 0;

  if (NewState)
  {
    NewState = false;
    i = 0;
    // return true;
  }

  if (firstPixelHue < 5*65536) 
  {
    for(int i=0; i<strip.numPixels(); i++) 
    {
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show();
    if (millis() - t >= wait)
    {
      firstPixelHue += 256;
      // digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
      t = millis();
    }
  }
  else
  {
    firstPixelHue = 0;
    return true;
  }
  return false;
}

// Rainbow-enhanced theater marquee. Pass delay time (in ms) between frames.
void theaterChaseRainbow(int wait) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in increments of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the strip (strip.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / strip.numPixels();
        uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show();                // Update strip with new contents
      delay(wait);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  }
}

void whiteOverRainbow(int whiteSpeed, int whiteLength) {

  if(whiteLength >= strip.numPixels()) whiteLength = strip.numPixels() - 1;

  int      head          = whiteLength - 1;
  int      tail          = 0;
  int      loops         = 3;
  int      loopNum       = 0;
  uint32_t lastTime      = millis();
  uint32_t firstPixelHue = 0;

  for(;;) { // Repeat forever (or until a 'break' or 'return')
    for(int i=0; i<strip.numPixels(); i++) {  // For each pixel in strip...
      if(((i >= tail) && (i <= head)) ||      //  If between head & tail...
         ((tail > head) && ((i >= tail) || (i <= head)))) {
        strip.setPixelColor(i, strip.Color(0, 0, 0, 255)); // Set white
      } else {                                             // else set rainbow
        int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
        strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
      }
    }

    strip.show(); // Update strip with new contents
    // There's no delay here, it just runs full-tilt until the timer and
    // counter combination below runs out.

    firstPixelHue += 40; // Advance just a little along the color wheel

    if((millis() - lastTime) > whiteSpeed) { // Time to update head/tail?
      if(++head >= strip.numPixels()) {      // Advance head, wrap around
        head = 0;
        if(++loopNum >= loops) return;
      }
      if(++tail >= strip.numPixels()) {      // Advance tail, wrap around
        tail = 0;
      }
      lastTime = millis();                   // Save time of last movement
    }
  }
}

void pulseWhite(uint8_t wait) {
  for(int j=0; j<256; j++) { // Ramp up from 0 to 255
    // Fill entire strip with white at gamma-corrected brightness level 'j':
    strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
    strip.show();
    delay(wait);
  }

  for(int j=255; j>=0; j--) { // Ramp down from 255 to 0
    strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
    strip.show();
    delay(wait);
  }
}

void rainbowFade2White(int wait, int rainbowLoops, int whiteLoops) {
  int fadeVal=0, fadeMax=100;

  // Hue of first pixel runs 'rainbowLoops' complete loops through the color
  // wheel. Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to rainbowLoops*65536, using steps of 256 so we
  // advance around the wheel at a decent clip.
  for(uint32_t firstPixelHue = 0; firstPixelHue < rainbowLoops*65536;
    firstPixelHue += 256) {

    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...

      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      uint32_t pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());

      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the three-argument variant, though the
      // second value (saturation) is a constant 255.
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue, 255,
        255 * fadeVal / fadeMax)));
    }

    strip.show();
    delay(wait);

    if(firstPixelHue < 65536) {                              // First loop,
      if(fadeVal < fadeMax) fadeVal++;                       // fade in
    } else if(firstPixelHue >= ((rainbowLoops-1) * 65536)) { // Last loop,
      if(fadeVal > 0) fadeVal--;                             // fade out
    } else {
      fadeVal = fadeMax; // Interim loop, make sure fade is at max
    }
  }

  for(int k=0; k<whiteLoops; k++) {
    for(int j=0; j<256; j++) { // Ramp up 0 to 255
      // Fill entire strip with white at gamma-corrected brightness level 'j':
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
    delay(1000); // Pause 1 second
    for(int j=255; j>=0; j--) { // Ramp down 255 to 0
      strip.fill(strip.Color(0, 0, 0, strip.gamma8(j)));
      strip.show();
    }
  }
}